---
title: اسنپ
last-check: 2019-04-14
href: https://snapp.ir
annouced: True
type: ugly
note: فقط در قسمت درخواست خودرو اپلیکیشن کپی‌رایت رعایت شده ولی در سایر موارد شامل وب اپ و زیرمجموعه های دیگر مانند اسنپ باکس و اسنپ فود و اسنپ مارکت رعایت نشده
android: https://play.google.com/store/apps/details?id=cab.snapp.passenger.play
---
